# Actually README #


# What features were added? #

### Non-Scripting: ###
* Level Block was expanded
* Jump pads themselves
* New UI elements 
* A death field

### Scripting: ###
* Player life count
* Player death upon falling 
* Player fail state 
* Proper player win state
* A button to reset to initial condition upon win or death 
* The ability to jump with the space bar
* Jump pads that help boost jumping power.  
* Some collectables move up and down using Lerp. 
* Player has a timer on the jump.

Also the game is winnable, the last one in the middle is just tricky on purpose. 
Hint: Use the jump pads to boost your momentum. It's in a cross shape for a reason. 

