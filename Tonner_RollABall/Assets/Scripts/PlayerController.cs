﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI; // Getting fancy apparently, need UI engine for button to work
using TMPro;

public class PlayerController : MonoBehaviour
{
    public float speed = 0; // Speed place holder variable

    public GameObject selfPlayer; // Self referntial variable

    public TextMeshProUGUI countText; // Text to replace count 

    public GameObject winTextObject;  // Text that appears after winning

    public GameObject loseTextObject;  // Text that appears after losing

    public TextMeshProUGUI lifeText; // A text for lives to be counted 

    public Button buttonTextObject; // Button for reseting game. 

    public GameObject buttonTextObject2; // Now just making it disappear 


    //Button was implemented using official Unity documentation 

    private Rigidbody rb; //The rigidbody for the ball

    private int count; // Count Variable for collectables

    private int lives; // Lives variable to keep track of player death 

    // To make the game properly reset, each pickup needs it's own variable to set unactive

    public GameObject pickUp1; // Needed to reset consumables, game object holder
    public GameObject pickUp2; // Needed to reset consumables, game object holder
    public GameObject pickUp3; // Needed to reset consumables, game object holder
    public GameObject pickUp4; // Needed to reset consumables, game object holder
    public GameObject pickUp5; // Needed to reset consumables, game object holder
    public GameObject pickUp6; // Needed to reset consumables, game object holder
    public GameObject pickUp7; // Needed to reset consumables, game object holder
    public GameObject pickUp8; // Needed to reset consumables, game object holder
    public GameObject pickUp9; // Needed to reset consumables, game object holder


    float jumpTimeLeft = 0f; // Timer for countdown between jumps


    private float movementX; // Used for the movement vector calculations for the X axis
    private float movementY; // Used for the movement vector calculations for the Y axis
    // Technically Y is really the Z axis in game

    // Start is called before the first frame update
    void Start()
    {
        // Activating the swarm of Pickups for reset
        pickUp1.SetActive(true);
        pickUp2.SetActive(true);
        pickUp3.SetActive(true);
        pickUp4.SetActive(true);
        pickUp5.SetActive(true);
        pickUp6.SetActive(true);
        pickUp7.SetActive(true);
        pickUp8.SetActive(true);
        pickUp9.SetActive(true);

        selfPlayer.SetActive(true); // To bring the player back after failure state 

        buttonTextObject.onClick.AddListener(TaskOnClick); 
        /* This is to make sure the button is going to respond to any events that could be called 
        while the game runs */

        rb = GetComponent<Rigidbody>(); // Rigid Body for Player 

        count = 0; // Setting the starting variable for count
        lives = 5; // Setting the starting variable for lives 

        SetCountText (); // Calling this function to update count in game

        SetLivesText (); // Calling this function to update the lives count in game

        winTextObject.SetActive(false); // Makes it so this doesn't appear until needed
        loseTextObject.SetActive(false); // Makes it so this doesn't appear until needed
        buttonTextObject2.SetActive(false); // Makes it so this doesn't appear until needed
    }

    private void OnMove(InputValue movementValue) // WASD  control for the Ball, not changed from tutorial 
    {
        Vector2 movementVector = movementValue.Get<Vector2>();
        movementX = movementVector.x;
        movementY = movementVector.y;
    }


    void Update()
    {
        if (jumpTimeLeft >= 0)
            jumpTimeLeft -= Time.deltaTime;

        if (jumpTimeLeft <= 0)
            if (Keyboard.current.spaceKey.wasPressedThisFrame) // Jump function relying on the tutorial input system
            {
                rb.AddForce(Vector3.up * 300f); // Up Vector speed for Jump 
                // Gravity kicks in for the downwards acceleration automatically 

                jumpTimeLeft = 1.5f; // Reset the timer
            }
    }


    private void FixedUpdate() // Still Tutorial movement code
    {
        Vector3 movement = new Vector3(movementX, 0.0f, movementY);

        rb.AddForce(movement * speed);
    }

    private void OnTriggerEnter(Collider other) // What allows the player to interact with the game world 
    {
        if (other.gameObject.CompareTag("PickUp")) // Tutorial pick ups 
        {
            other.gameObject.SetActive(false);

            count += 1;

            SetCountText();
 
        }
        if (other.gameObject.CompareTag("BoostPad")) // Implementing Boost pads to allow a super jump
        {
            rb.AddForce(Vector3.up * 500f); // Just some added jump
        }
        
        if (other.gameObject.CompareTag("Death")) // For when the play falls during parkour 
        {
            lives -= 1; // Player now has lost a life for "dying"

            SetLivesText(); // Update life text for new lives 

            transform.localPosition = new Vector3(0.43f, 0.55f, 0); // Brings the player back to the starting position

            // Kills player momentum, mostly, to have them not die repeatedly
            rb.angularVelocity = Vector3.zero; 
            rb.velocity = Vector3.zero; 
          
            // Found idea to use velocity as the correct Vector online 
        }

    }

    void TaskOnClick() // This is the event called that the buttons is listening for. Allows the button to be clicked. 
    {

        // Kills player momentum, mostly, to have them not die repeatedly 
        rb.angularVelocity = Vector3.zero; 
        rb.velocity = Vector3.zero;

        transform.localPosition = new Vector3(0.43f, 0.55f, 0); //Reset player position 

        Start(); // To Reset most things just calling the start variavle 
    }

    void SetCountText() // Sets the count of the collectables in the UI when called 
    {
        countText.text = "Collectables: " + count.ToString(); // Where the function actually changes the UI Text 

        if (count >= 9) // Used to know when the game has been won
        {
            //Activating the win state by stopping the player and bringing victory text and the reset button.
            winTextObject.SetActive(true); 
            buttonTextObject2.SetActive(true);
            selfPlayer.SetActive(false);
        }
    }

    void SetLivesText() // Sets the count of the lives at the start and when the player dies 
    {
        lifeText.text = "Lives: " + lives.ToString(); // Where the function decreases the player life count 

        if (lives <= 0) // How to know when the player has actually lost from decreasing the lives 
        {
            //Activating the fail state by stopping the player and bringing lose text and the reset button.
            loseTextObject.SetActive(true);
            buttonTextObject2.SetActive(true);
            selfPlayer.SetActive(false); 
        }
    }

}

