﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveBackAndForth : MonoBehaviour // Adding in vertical back and forth
{
    public Vector3 moveTo; // Set the varaible for how much and where the object will move
    public float speed = 0.5f; // The multipler for what speed it will move

    Vector3 beginPos; // Where the object begins
    Vector3 endPos; // Where the object will end

    void Start()
    {
        beginPos = transform.position; // Setting the object to it's current position dynamically 
        endPos = beginPos + moveTo; // Now applying where you want the object to move to to the end variable 
    }


    void Update()
    {
        float progress = Mathf.PingPong(Time.time * speed, 1f); // Sets progress according to the multiplier and time
        // This goes between 0-1 to create a constantly changing variable through the multiplier and Time.time 

        transform.position = Vector3.Lerp(beginPos, endPos, progress); 
        // Using Linear Interpolation we can have the game engine move the object in a simple line by itself
        /* Uses the first Vector of where it is, then the Vector of where you want it, and progress (really time)
        to determine how to move it and where to. */
    }
}